-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.33 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for chomchom
CREATE DATABASE IF NOT EXISTS `chomchom` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `chomchom`;

-- Dumping structure for table chomchom.admin
CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(30) COLLATE utf8_unicode_ci DEFAULT '',
  `password` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table chomchom.admin: ~0 rows (approximately)
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
REPLACE INTO `admin` (`id`, `account`, `password`) VALUES
	(1, 'admin', 'admin');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;

-- Dumping structure for table chomchom.items
CREATE TABLE IF NOT EXISTS `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_name` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table chomchom.items: ~6 rows (approximately)
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
REPLACE INTO `items` (`id`, `item_name`) VALUES
	(1, 'Games'),
	(2, 'Tin tức'),
	(3, 'Giới thiệu'),
	(4, 'Dịch vụ'),
	(5, 'Liên hệ'),
	(6, 'Hướng dẫn');
/*!40000 ALTER TABLE `items` ENABLE KEYS */;

-- Dumping structure for table chomchom.posts
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `itemid` int(11) DEFAULT NULL,
  `title` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` varchar(10000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `delete` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `posts_fi_4a4f3b` (`itemid`),
  CONSTRAINT `posts_fk_4a4f3b` FOREIGN KEY (`itemid`) REFERENCES `items` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table chomchom.posts: ~19 rows (approximately)
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
REPLACE INTO `posts` (`id`, `itemid`, `title`, `content`, `note`, `image`, `date_create`, `delete`) VALUES
	(1, 1, 'Game mới', '   Game hay mới ra mắt đã thu hút rất nhiều game thủ ', '  ahihi', NULL, '2019-08-26 11:53:29', 0),
	(2, 2, 'Thông báo bảo trì định kỳ', 'Thông báo bảo trì định kỳ', NULL, NULL, '2019-08-22 09:21:19', 1),
	(3, 3, 'Bảo trì hệ thống nạp tiền đối vơi thẻ Mobiphone', 'Bảo trì hệ thống nạp tiền đối vơi thẻ Mobiphone', NULL, NULL, '2019-08-13 03:18:15', 1),
	(4, 4, 'Thông báo bảo trì định kỳ', 'Thông báo bảo trì định kỳ', NULL, NULL, '2019-08-12 06:26:27', 1),
	(5, 5, 'Hướng dẫn nạp thẻ trên chomchom', 'Hướng dẫn nạp thẻ trên chomchom', NULL, NULL, '2019-08-09 11:49:54', 1),
	(6, 1, 'Game Vua tam quốc mới ra mắt', 'Game Vua tam quốc mới ra mắt', '    ', '["bia6.jpg"]', '2019-08-26 10:40:05', 1),
	(7, 1, 'Game Vua tam quốc mới ra mắt', 'Game Vua tam quốc mới ra mắt', '    ', '["bia6.jpg"]', '2019-08-26 10:40:22', 1),
	(8, 1, 'Game Vua tam quốc mới ra mắt', 'Game Vua tam quốc mới ra mắt', '    ', '["bia5.jpg"]', '2019-08-26 10:42:19', 1),
	(9, 1, 'Game Vua tam quốc mới ra mắt', 'Game Vua tam quốc mới ra mắt', '  ', '["bia5.jpg"]', '2019-08-26 10:44:26', 1),
	(10, 1, 'Game Vua tam quốc mới ra mắt', 'Game Vua tam quốc mới ra mắt', '  ', '["bia5.jpg"]', '2019-08-26 10:46:18', 1),
	(11, 3, 'fdfdfd', 'adsdds', '  ', '["g.jpg"]', '2019-08-26 10:48:35', 1),
	(12, 3, 'fdfdfd', 'adsdds', '  ', '["g.jpg"]', '2019-08-26 10:49:06', 1),
	(13, 1, 'Game Vua tam quốc mới ra mắt', 'Game Vua tam quốc mới ra mắt', '    ', '["bia5.jpg"]', '2019-08-26 10:50:23', 1),
	(14, 6, 'Hướng dẫn nạp thẻ trên chomchom', 'Hướng dẫn nạp thẻ trên chomchom', '    ', '["bia5.jpg"]', '2019-08-26 10:55:58', 1),
	(15, 6, 'Làm thế nào để chơi game', 'Làm thế nào để chơi game', '  ', '["bia5.jpg"]', '2019-08-26 10:58:37', 1),
	(16, 6, 'Hướng dẫn nạp thẻ trên chomchom', 'Hướng dẫn nạp thẻ trên chomchom', '    ', '["bia5.jpg"]', '2019-08-26 11:02:41', 1),
	(17, 3, 'Hướng dẫn nạp thẻ trên chomchom', '<p>This is my textarea to be replaced with CKEditor.<img alt="" src="https://quayeuthuong.vn/vnt_upload/news/12_2017/shop_gau_bong_gia_re_tai_hcm_va_ha_noi_5.jpg" style="height:200px; width:200px" /></p>\r\n', '  ', '["bia5.jpg"]', '2019-08-27 09:05:12', 1),
	(18, 1, 'Game liên quân mobie', '<p>&nbsp;</p>\r\n\r\n<p><img alt="" src="http://nhandaovadoisong.com.vn/wp-content/uploads/2019/05/hinh-anh-lien-quan-mobile-dep-nhat-lam-hinh-nen-1.jpg" style="border-style:solid; border-width:1px; height:110px; margin:10px 20px; width:196px" /></p>\r\n\r\n<p>ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas feugiat consequat diam. Maecenas metus. Vivamus diam purus, cursus a, commodo non, facilisis vitae, nulla. Aenean dictum lacinia tortor. Nunc iaculis, nibh non iaculis aliquam, orci felis euismod neque, sed ornare massa mauris sed velit. Nulla pretium mi et risus. Fusce mi pede, tempor id, cursus ac, ullamcorper nec, enim. Sed tortor. Curabitur molestie. Duis velit augue, condimentum at, ultrices a, luctus ut, orci. Donec pellentesque egestas eros. Integer cursus, augue in cursus faucibus, eros pede bibendum sem, in tempus tellus justo quis ligula. Etiam eget tortor. Vestibulum rutrum, est ut placerat elementum, lectus nisl aliquam velit, tempor aliquam eros nunc nonummy metus. In eros metus, gravida a, gravida sed, lobortis id, turpis. Ut ultrices, ipsum at venenatis fringilla, sem nulla lacinia tellus, eget aliquet turpis mauris non enim. Nam turpis. Suspendisse lacinia. Curabitur ac tortor ut ipsum egestas elementum. Nunc imperdiet gravida mauris.</p>\r\n', '', '[]', '2019-08-29 16:37:12', 1),
	(19, 2, 'Thông báo mở thêm hình thức nạp Đĩnh bằng thẻ GOSU và SMS Viettel', '<p>Th&ocirc;ng b&aacute;o đến qu&yacute; kh&aacute;ch h&agrave;ng!</p>\r\n\r\n<p>H&igrave;nh thức nạp&nbsp;ĐĨNH&nbsp;bằng thẻ GOSU v&agrave; SMS đầu số Viettel đ&atilde; ch&iacute;nh thức được đưa v&agrave;o hoạt động. Qu&yacute; kh&aacute;ch c&oacute; thể dễ d&agrave;ng quy đổi ĐĨNH th&ocirc;ng qua 2 h&igrave;nh thức nạp tr&ecirc;n.&nbsp;<br />\r\n&nbsp;- Xem hướng dẫn nạp ĐĨNH bằng thẻ GOSU tại&nbsp;<a href="https://id.duo.vn/News/Details/469" target="_blank">đ&acirc;y</a>.<br />\r\n&nbsp;- Xem hướng dẫn nạp ĐĨNH bằng SMS đầu số Viettel tại&nbsp;<a href="https://id.duo.vn/News/Details/470" target="_blank">đ&acirc;y</a>.</p>\r\n\r\n<p><img alt="" src="https://i.imgur.com/OsHC9Fd.png" style="height:318px; margin:10px 40px; width:400px" /></p>\r\n\r\n<p>Qu&yacute; kh&aacute;ch h&agrave;ng cũng c&oacute; thể thực hiện nạp ĐĨNH bằng c&aacute;c loại h&igrave;nh đang hiện h&agrave;nh tr&ecirc;n trang: thẻ VTC, thẻ FPT v&agrave; thẻ BIT.</p>\r\n', NULL, NULL, '2019-08-30 04:07:07', 1);
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;

-- Dumping structure for table chomchom.user
CREATE TABLE IF NOT EXISTS `user` (
  `makh` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateregister` date DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `gender` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(500) COLLATE utf8_unicode_ci DEFAULT '',
  `describe` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` tinyint(4) DEFAULT NULL,
  `delete` tinyint(4) DEFAULT NULL,
  `phonenumber` varchar(30) COLLATE utf8_unicode_ci DEFAULT '',
  `avatar` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`makh`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table chomchom.user: ~16 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
REPLACE INTO `user` (`makh`, `name`, `account`, `password`, `email`, `dateregister`, `birthday`, `gender`, `address`, `describe`, `state`, `delete`, `phonenumber`, `avatar`, `code`) VALUES
	(1, 'Nguyễn Hạnh hihi', 'hanhng', 'hanhkute', 'hanhnguyen060497@gmail.com', '2019-08-24', '2019-08-12', 'Nữ', 'Quảng Trị', '', 0, 1, '3892781', 'ji.jpeg', NULL),
	(2, NULL, 'ahihi', '', 'ahihi@gmail.com', NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, 'avatar.png', NULL),
	(3, '', 'hanhng1', '12345', 'phuongnha@gmail.com', '2019-08-29', NULL, NULL, '', NULL, 0, 1, '', NULL, NULL),
	(4, '', 'hanhng2', '123123', 'hanhnguyen060497@gmail.com', '2019-08-29', NULL, NULL, '', NULL, 0, 1, '', NULL, NULL),
	(5, '', 'hanhng3', '123123', 'hanhnguyen060497@gmail.com', '2019-08-29', NULL, NULL, '', NULL, 0, 1, '', NULL, NULL),
	(6, '', 'hanhng4', '123123', 'hanhnguyen060497@gmail.com', '2019-08-29', NULL, NULL, '', NULL, 0, 1, '', NULL, NULL),
	(7, '', 'hanhng6', '123123', 'hanhnguyen060497@gmail.com', '2019-08-29', NULL, NULL, '', NULL, 0, 1, '', NULL, NULL),
	(8, '', 'hanhng7', '123123', 'hanhn97@gmail.com', '2019-08-29', NULL, NULL, '', NULL, 0, 1, '', NULL, NULL),
	(9, '', 'hanhng8', '123123', 'hanhn9797@gmail.com', '2019-08-29', NULL, NULL, '', NULL, 0, 1, '', NULL, NULL),
	(10, '', 'hanhng9', '123123', 'hanh97@gmail.com', '2019-08-29', NULL, NULL, '', NULL, 0, 1, '', NULL, NULL),
	(11, '', 'hanhng11', '123123', 'hanh977@gmail.com', '2019-08-29', NULL, NULL, '', NULL, 0, 1, '', NULL, NULL),
	(12, '', 'hanhng12', '123123', 'hanhcndsaf@gmail.com', '2019-08-29', NULL, NULL, '', NULL, 0, 1, '', NULL, NULL),
	(13, '', 'hanhng14', '123123', 'hanhcnb@gmail.com', '2019-08-29', NULL, NULL, '', NULL, 0, 1, '', NULL, NULL),
	(14, '', 'hanhngsfdgh', '123123', 'hanhcdzvc@gmail.com', '2019-08-29', NULL, NULL, '', NULL, 0, 1, '', NULL, NULL),
	(15, '', 'hanhkkk', '123123', 'phfdfda@gmail.com', '2019-08-29', NULL, NULL, '', NULL, 0, 1, '', NULL, NULL),
	(16, '', 'hanhng15', '123123', 'pfdgds@gmail.com', '2019-08-29', NULL, NULL, '', NULL, 0, 1, '', NULL, NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
