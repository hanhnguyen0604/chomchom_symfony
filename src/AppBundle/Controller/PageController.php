<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use AppBundle\Model\UserQuery;
use AppBundle\Model\AdminQuery;
use AppBundle\Model\Admin;
use AppBundle\Model\User;
use AppBundle\Model\ItemsQuery;
use AppBundle\Model\Items;
use AppBundle\Model\PostsQuery;
use AppBundle\Model\Posts;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
class PageController extends Controller
{
	 /**
     * @Route("/info_page", name="info_page")
     */
    public function infoPageAction(Request $request){
    	$posts=PostsQuery::create()->filterByItemid(2)->filterByDelete(true);
    	    	//$this->view_data['posts']=$posts;
    	$page = $request->query->get('page', 1);
        $limit  = 10;
        $info_pagi = $posts->paginate($page, $limit);
        //dump($info_pagi);die;
         $this->view_data['info_pagi']=$info_pagi;
    	return $this->render('@App/info_page.html.twig',$this->view_data);
    }
    /**
     * @Route("/guide_page", name="guide_page")
     */
    public function guidePageAction(Request $request){
    	$posts=PostsQuery::create()->filterByItemid(6)->filterByDelete(true);
    	   
    	$page = $request->query->get('page', 1);
        $limit  = 10;
        $info_pagi = $posts->paginate($page, $limit);
        //dump($info_pagi);die;
         $this->view_data['info_pagi']=$info_pagi;
    	return $this->render('@App/guide_page.html.twig',$this->view_data);
    }
    /**
     * @Route("/info_sufficient/{id}", name="info_sufficient")
     */
    public function infoSufficientAction(Request $request,$id){


    	$session=new Session();
        $post=PostsQuery::create()->filterById($id)->findOne();
        $item= ItemsQuery::create()->filterById($post->getItemid())->findOne()->getItemName();
        $this->view_data['post']=$post;
        $this->view_data['item']= $item;
        return $this->render('@App/info_sufficient.html.twig',$this->view_data);
    }
    /**
     * @Route("/active_email", name="active_email")
     */
    public function activeEmailAction(Request $request,$id){

    	$session=new Session();
       
        return $this->render('@App/index.html.twig');
    }
    /**
     * @Route("/activate/{id}", name="activate")
     */
    public function activateAction(Request $request,$id){

    	$session=new Session();
    	 if($session->get('account')!=null){
        $this->view_data['acc']=$session->get('account');
        $this->view_data['makh']=$session->get('makh');
        $this->view_data['date']=$session->get('date');
        $this->view_data['name']=$session->get('name');
        $this->view_data['state']=$session->get('state');
        $user=UserQuery::create()->filterByAccount($session->get('account'))->findOne();
       
        if($user->getCode()==$id){
        
        	$user->setState(true);
        	$user->save();
        	$session->set('state',1);
        	$session->getFlashBag()->add('advertise','Bạn đã kích hoạt bằng Email thành công !');
        }

         return $this->redirectToRoute('homepage');
    }

        $session->getFlashBag()->add('advertise','Bạn cần đăng nhập trước khi kích hoạt !');
        return $this->render('@App/index.html.twig');
    }
 }