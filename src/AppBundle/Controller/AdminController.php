<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use AppBundle\Model\UserQuery;
use AppBundle\Model\AdminQuery;
use AppBundle\Model\Admin;
use AppBundle\Model\User;
use AppBundle\Model\ItemsQuery;
use AppBundle\Model\Items;
use AppBundle\Model\PostsQuery;
use AppBundle\Model\Posts;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
class AdminController extends Controller
{
    /**
     * @Route("/admin", name="admin")
     */
    public function adminAction(Request $request)
    {
    	$session=new Session();
       
    	if($request->isMethod('POST')){
    		
    		$errors=array();
    		$acc=trim($request->get('acc_admin'));
    		$admin=AdminQuery::create()->filterByAccount($acc)->findOne();
    		
    		if($acc== null){
    			$errors['acc']='Tài khoản trống !';
			}
			if($admin== null){
    			$errors['acc']='Sai tài khoản !';
			}
			if($request->get('pass_admin') == null){
    			$errors['pass']='Mật khẩu trống !';
			}
			if($admin != null &&  $admin->getPassword() !=$request->get('pass_admin')){
    			$errors['pass']='Sai mật khẩu !';
			}
			//dump('1');die;
    		if(!$errors){
    			$session->set('acc_admin',$admin->getAccount());
    			$session->set('pass_admin',$admin->getPassword());
                $page = $request->query->get('page', 1);
                $limit  = 10;
                $user = UserQuery::create()->filterByDelete(true);
                $user_pagi = $user->paginate($page, $limit);
                $notify=$session->getFlashBag()->get('notify');
                $this->view_data['user_pagi']=$user_pagi;
    			$this->view_data['acc_admin']=$acc;
    		    $this->view_data['pass_admin']=$request->get('pass_admin');
    			return $this->render('@App/admin.html.twig',$this->view_data);
    		}
    		//dump('3');die;
    		   $this->view_data['errors']=$errors;
    			$this->view_data['acc_admin']=$request->get('acc_admin');
    		return $this->render('@App/admin_login.html.twig',$this->view_data);
    	}
    	if($session->get('acc_admin')){
            $page = $request->query->get('page', 1);
            $limit  = 10;
            $user = UserQuery::create()->filterByDelete(true);
            $user_pagi = $user->paginate($page, $limit);
            $notify=$session->getFlashBag()->get('notify');
            $this->view_data['user_pagi']=$user_pagi;
    		$this->view_data['acc_admin']=$session->get('acc_admin');
    		$this->view_data['pass_admin']=$session->get('pass_admin');
    		return $this->render('@App/admin.html.twig',$this->view_data);
    	}
    	return $this->render('@App/admin_login.html.twig');

    }
     /**
     * @Route("/list_user", name="list_user")
     */
    public function viewListAction(Request $request)
    {  
        $page = $request->query->get('page', 1);
       if($request->query->get('page')){
         $page = $request->query->get('page');
       }
        $limit  = 10;
        $user = UserQuery::create()->filterByDelete(true);
        $user_pagi = $user->paginate($page, $limit);

        return $this->render('@App/admin_templates/list_user.html.twig',['user_pagi' => $user_pagi,'page' => $page]);
         
    }
    /**
     * @Route("/logout_admin", name="logout_admin")
     */
    public function logoutAction(Request $request)
    {   
        $session=new Session;
        $session->remove('acc_admin');
        $session->remove('pass_admin');
        return $this->redirectToRoute('admin'); 
    }
    /**
     * @Route("/admin/user_info/{makh}", name="user_info")
     */
    public function infoUserAction(Request $request,$makh)
    {
        $session=new Session();
        $user=UserQuery::create()->filterByMakh($makh)->findOne();
        $this->view_data['user']=$user;
        if($session->get('acc_admin')== null ){
           return  $this->redirectToRoute('admin');
        }
        return $this->render('@App/info_user.html.twig',$this->view_data);
    }
     /**
     * @Route("/user/delete/{id}", name="user_del")
     */
     public function deleteUserAction($id){
        $user = UserQuery::create()->filterByMakh($id)->findOne();
        if($user!=null){
            $user->setDelete(false);
            $user->save(); 
            echo json_encode(array(
                'Mess' => 'Bạn đã xóa thành công !',
                'Code' => 1
            ),JSON_UNESCAPED_UNICODE);
            exit();

        }
        echo json_encode(array(
                'Mess' => 'Bạn đã xóa thất bại !',
                'Code' => 2
            ),JSON_UNESCAPED_UNICODE);
         exit();
     }
     /**
     * @Route("/search", name="search")
    */
    public function searchAction(Request $request)
    { 
        $key = $request->query->get('name');

        $value=explode(' ', $key);
        $users  = UserQuery::create()
        ->where('user.account like ?','%'.$key.'%')
        ->_or()
        ->where('user.name like ?','%'.$key.'%')
        ->_or()
        ->where('user.email like ?','%'.$key.'%');
        $page = $request->query->get('page', 1);
        $limit  = 10;
        $user_pagi = $users->paginate($page, $limit);
        
        $e=$users->find();
        $error=1;
        if(empty($e->toArray())){
          $error=0;
        }
        return $this->render('@App/admin_templates/list_user.html.twig',['user_pagi' => $user_pagi,'page' => $page,'error'=>$error]);
    }
    /**
     * @Route("/admin/posts", name="posts")
    */
    public function postsAction(Request $request)
    { 
        $session=new Session();
        $id=1;
        if( $request->query->get('id')){
           $id= $request->query->get('id') ;  
        }
        $page = $request->query->get('page', 1);
        $limit  = 10;
        $items=ItemsQuery::create()->find();
        $posts =PostsQuery::create()->filterByDelete(true)->filterByItemid($id);
        $user_pagi = $posts->paginate($page, $limit);
        $notify=$session->getFlashBag()->get('notify');
        $this->view_data['user_pagi']=$user_pagi;
        $this->view_data['items']=$items;
        $this->view_data['acc_admin']=$session->get('acc_admin');
        $this->view_data['pass_admin']=$session->get('pass_admin');
        
        return $this->render('@App/posts.html.twig',$this->view_data);
        if($session->get('acc_admin')==null)
             return $this->redirectToRoute('admin');
     }
      /**
     * @Route("/list_post", name="list_post")
     */
    public function viewListPostAction(Request $request)
    {  $session=new Session();
        $id=1;
        if( $request->query->get('id')){
           $id= $request->query->get('id') ;  
        }
       
        $page = $request->query->get('page', 1);
       if($request->query->get('page')){
         $page = $request->query->get('page');
       }
        $limit  = 10;
        $items=ItemsQuery::create()->find();
        $posts =PostsQuery::create()->filterByDelete(true)->filterByItemid($id);
      
        $user_pagi = $posts->paginate($page, $limit);
        $this->view_data['user_pagi']=$user_pagi;
        $this->view_data['items']=$items;
        $this->view_data['page']=$page;
        if($session->get('acc_admin')== null ){
           return  $this->redirectToRoute('admin');
        }

        return $this->render('@App/admin_templates/list_post.html.twig', $this->view_data);
         
    } 
    /**
     * @Route("/admin/post_add", name="post_add")
     */
    public function addPostAction(Request $request){
        $session=new Session;
         $items=ItemsQuery::create()->find();
         $this->view_data['items']=$items;
         if ($request->isMethod('POST')) {
            $post=new Posts();

            $post->setTitle(($request->get('tieude')!=null)?$request->get('tieude'):'');
            $post->setItemid(($request->get('muc')!=null)?$request->get('muc'):'');
            $post->setContent(($request->get('editor1')!=null)?$request->get('editor1'):'');
            $post->setDescribe(($request->get('mota')!=null)?$request->get('mota'):'');
            $post->setDateCreate(date('Y-m-d H:i:s'));
            $post->setDelete(true);
            
            $errors=array();
            if($request->get('tieude')==''){
                $errors['tieude']='Đây là trường bắt buộc !';
            }
            if($request->get('editor1')==''){
                $errors['noidung']='Đây là trường bắt buộc !';
            }
            if($request->get('mota')==''){
                $errors['mota']='Đây là trường bắt buộc !';
            }
            if(strlen($request->get('mota'))>1000){
                $errors['mota']='Trường mô tả không được quá 1000 ký tự !';
            }
            if(strlen($request->get('editor1'))>2800){
                $errors['noidung']='Trường Nội dung không được quá 2800 ký tự !';
            }
            if(strlen($request->get('tieude'))>200){
                $errors['tieude']='Tiêu đề không quá 200 ký tự !';
            }
            if(!$errors){
            
                $post->save();
                $notify="Bạn đã thêm bài đăng thành công !";
                $session->getFlashBag()->add('notify', $notify);
                return $this->redirectToRoute('posts');
              
            }
                $this->view_data['errors']=$errors;
                $this->view_data['muc']=$request->get('muc');
                $this->view_data['post']=$post;
                return $this->render('@App/add_post.html.twig',$this->view_data);
            
            
         }
         if($session->get('acc_admin')== null ){
           return  $this->redirectToRoute('admin');
        }
        
   return $this->render('@App/add_post.html.twig',$this->view_data);
    }
    /**
     * @Route("/admin/post_edit/{id}", name="post_edit")
     */
    public function editPostAction(Request $request,$id){
        $session=new Session;
       
        $post= PostsQuery::create()->filterById($id)->findOne();

         $items=ItemsQuery::create()->find();
        $this->view_data['post']=$post;
        $this->view_data['items']=$items;
        if ($request->isMethod('POST')) {
            $post->setTitle(($request->get('tieude')!=null)?$request->get('tieude'):$post->getTitle());
            $post->setContent(($request->get('editor1')!=null)?$request->get('editor1'):$post->getContent());
            $post->setDescribe(($request->get('mota')!=null)?$request->get('mota'):$post->getDescribe());

           
            $errors=array();
            if(strlen($request->get('mota'))>1000){
                $errors['mota']='Trường mô tả không được quá 1000 ký tự !';
            }
            if(strlen($request->get('editor1'))>2800){
                $errors['noidung']='Trường Nội dung không được quá 2800 ký tự !';
            }
            if(strlen($request->get('tieude'))>200){
                $errors['tieude']='Tiêu đề không quá 200 ký tự !';
            }
           
           
            if(!$errors){
            
                $post->save();
                $notify="Bạn đã sửa bài viết thành công !";
                $session->getFlashBag()->add('notify', $notify);
                return $this->redirectToRoute('posts');
               
            }else{
                $this->view_data['errors']=$errors;
                
                
                return $this->render('@App/edit_post.html.twig',$this->view_data);}
            
            
         }
         if($session->get('acc_admin')== null ){
           return  $this->redirectToRoute('admin');
        }
        
   return $this->render('@App/edit_post.html.twig',$this->view_data);
    }
    /**
     * @Route("/admin/post_info/{id}", name="admin_post_info")
     */
    public function infoPostAction(Request $request,$id)
    {
        $session=new Session();
        $post=PostsQuery::create()->filterById($id)->findOne();
        $item= ItemsQuery::create()->filterById($post->getItemid())->findOne()->getItemName();
        $this->view_data['post']=$post;
        $this->view_data['item']= $item;
        if($session->get('acc_admin')== null ){
           return  $this->redirectToRoute('admin');
        
        }
      
        return $this->render('@App/info_post.html.twig',$this->view_data);
    }

     /**
     * @Route("/post/delete/{id}", name="post_del")
     */
     public function deletePostAction($id){
        $post = PostsQuery::create()->filterById($id)->findOne();
        if($post!=null){
            $post->setDelete(false);
            $post->save(); 
            echo json_encode(array(
                'Mess' => 'Bạn đã xóa thành công !',
                'Code' => 1
            ),JSON_UNESCAPED_UNICODE);
            exit();

        }
        echo json_encode(array(
                'Mess' => 'Bạn đã xóa thất bại !',
                'Code' => 2
            ),JSON_UNESCAPED_UNICODE);
         exit();
     }
     /**
     * @Route("/editor", name="editor")
     */
    public function editorAction(Request $request)
    {
        return $this->render('@App/editors.html.twig');
    }
}