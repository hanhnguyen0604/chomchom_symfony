<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use AppBundle\Model\UserQuery;
use AppBundle\Model\User;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
       
          $session = new Session();
         
          if($request->isMethod('POST')){
          
            $errors=array();
            if($request->get('account')==null){
                $errors['account']='Hãy nhập tên đăng nhập của bạn ! ';
            }
            

             $acc=trim($request->get('account'));
            $user= UserQuery::create()->filterByAccount($acc)->findOne();
 
            if($user==null)
                 $errors['account']='Sai tên đăng nhập';

            if($user!=null && $user->getPassword()!= $request->get('password'))
                $errors['password']='Sai mật khẩu !';
            if($request->get('password')== '')

            $errors['password']='Hãy nhập mật khẩu !';
            if(!$errors){
                $session->set('account', $user->getAccount());
                $session->set('makh',$user->getMakh());
                $session->set('name', $user->getName());
                $session->set('date', $user->getDateregister());
                $session->set('state', $user->getState());

                 $advertise=$session->getFlashBag()->get('advertise');
                $this->view_data['acc']=$session->get('account');
                $this->view_data['makh']=$session->get('makh');
                $this->view_data['date']=$session->get('date');
                $this->view_data['name']=$session->get('name');
                $this->view_data['state']=$session->get('state');
               
                $p=40;
                if($user->getName()!='')
                    $p=$p+7.5;
                if($user->getGender()!='')
                    $p=$p+7.5;
                if($user->getBirthday()!='')
                    $p=$p+7.5;
                if($user->getAvatar()!='')
                    $p=$p+7.5;
                if($user->getDescribe()!='')
                    $p=$p+7.5;
                if($user->getAddress()!='')
                    $p=$p+7.5;
                if($user->getPhonenumber()!='')
                    $p=$p+7.5;
                if($user->getState()==1)
                    $p=$p+7.5;
                if($p!=100){
                  $session->getFlashBag()->add('advertise','Thông tin cá nhân của bạn còn thiếu, hãy cập nhật đầy đủ !');
                  $advertise=$session->getFlashBag()->get('advertise');
                  $this->view_data['advertise']= $advertise[0];
                }
        
            return $this->render('@App/index.html.twig',$this->view_data);

          }
          
           return $this->render('@App/index.html.twig',['error'=>$errors,'account'=>$request->get('account')]);
           
       }
       if($session->get('account')!=null){
       // dump($session->getFlashBag()->get('advertise'));die;

        $advertise=$session->getFlashBag()->get('advertise');
        $this->view_data['acc']=$session->get('account');
        $this->view_data['makh']=$session->get('makh');
        $this->view_data['date']=$session->get('date');
        $this->view_data['name']=$session->get('name');
        $this->view_data['state']=$session->get('state');


        if($advertise)
            $this->view_data['advertise']= $advertise[0];
        return $this->render('@App/index.html.twig', $this->view_data);
       }
        
    return $this->render('@App/index.html.twig');
    }
   
    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction(Request $request)
    {
        $session = new Session();

            $session->remove('account');
            $session->remove('makh');
            $session->remove('date');
            $session->remove('name');
       
        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/captcha", name="captcha")
     */
    public function captcha()
    {
        //captcha
            $session = new Session();
            $code=rand(1000,9999); 
            $session->set('code', $code); 
            $im = imagecreatetruecolor(50, 24); 
            $bg = imagecolorallocate($im, 225, 225, 225); //background color 
            $fg = imagecolorallocate($im, 255, 0, 0);//text color 
            for($i=0;$i<20;$i++)
                imageline ( $im , rand(0,100) , rand(0,100) , rand(0,100),rand(0,100),rand(0,100000));
            imagefill($im, 0, 0, $bg); 
            imagestring($im, 5, 5, 5, $code, $fg); 
            header("Cache-Control: no-cache, must-revalidate"); 
            header('Content-type: image/png'); 
            imagepng($im); 
            imagedestroy($im); 
           
        //captcha
    }
     /**
     * @Route("/errorFomat/{acc}", name="errorFomat")
     */
    public function errorFomat($acc)
    {    
        if (!preg_match("/^[a-zA-Z0-9_ ]*$/",$acc)) {
            echo 'Tên đăng nhập chỉ bao gồm chữ số từ 0-9,_ và chữ cái a-z,A-Z';
            exit();
        }
        else if(strlen($acc)<6 || strlen($acc)>20){
            echo 'Tên đăng nhập phải từ 6-20 kí tự';
            exit();
        }
        else{
            echo '';
            exit();
        }
             
        return 0;
    }
    
     /**
     * @Route("/changepass", name="changepass")
     */
    public function changepass(Request $request)
    {    
      if($request->isMethod('POST')){
        $session = new Session();
        $err=0;

        $u=UserQuery::create()->filterByAccount($session->get('account'))->findOne();

        $this->view_data['makh']=$u->getMakh();
        $this->view_data['name']=$u->getName();
        $this->view_data['date']=$u->getDateregister();
        $this->view_data['acc']=$session->get('account');
        $this->view_data['state']=$u->getCode();

        if($request->get('passold') !=$u->getPassword()){
          $err=1;
        }
        if($request->get('passold')=='' )
            $err=1;
        if($request->get('passnew')=='' )
            $err=1;
        if($request->get('confirmpassnew')=='' )
            $err=1;

        if(strlen($request->get('passnew')) <5 || strlen($request->get('passnew')) >30 ){
          $err=1;
        }
        if($request->get('confirmpassnew')!= $request->get('passnew')){
               $err=1;
       
        }
          

        if($err==0){
            $u->setPassword($request->get('passnew'));
            $u->save();
            $session->getFlashBag()->add('advertise','Bạn đã đổi mật khẩu thành công ! ');
         
            
            return $this->redirectToRoute('homepage');
        }
            $session->getFlashBag()->add('advertise','Đổi mật khẩu không thành công ! ');
            return $this->redirectToRoute('homepage');
        
        
      }
      
       return $this->render('@App/index.html.twig',$this->view_data);
     
    }
    /**
     * @Route("/passwordold", name="passwordold")
     */
    public function passold(Request $request)
     {   $session=new Session();
       
        $u=UserQuery::create()->filterByAccount($session->get('account'))->findOne();
       $po=$request->query->get('po');
       if($po!=$u->getPassword() ){
         echo json_encode(array(
                'Mess' => 'Sai mật khẩu ! ',
                'Style' => 'border:red solid 1px'
            ),JSON_UNESCAPED_UNICODE);
            exit();

         
        }
        else
           echo json_encode(array(
                'Mess' => '',
                'Style' => 'border:green solid 1px'
            ),JSON_UNESCAPED_UNICODE);
            exit();
    } 
     /**
     * @Route("/passwordnew", name="passwordnew")
     */
    public function passnew(Request $request)
     {   
       $pn=$request->query->get('pn');
       $po=$request->query->get('po');
       if($pn==$po){
         echo json_encode(array(
                'Mess' => 'Bạn đã nhập lại mật khẩu cũ !',
                'Color'=>'color:orange',
                'Style' => 'border:green solid 1px;'
            ),JSON_UNESCAPED_UNICODE);
          exit();
       }
       if(strlen($pn)<5|| strlen($pn)>30 ){
           echo json_encode(array(
                'Mess' => 'Mật khẩu phải từ 5-30 ký tự !',
                'Color'=>'',
                'Style' => 'border:red solid 1px'
            ),JSON_UNESCAPED_UNICODE);
          exit();
        }
        else
           echo json_encode(array(
                'Mess' => '',
                'Color'=>'',
                'Style' => 'border:green solid 1px'
            ),JSON_UNESCAPED_UNICODE);
            exit();
    }
    /**
     * @Route("/confirmpass", name="confirmpass")
     */
    public function confirmpass(Request $request)
     {  
        $pn=$request->query->get('pn');
        $pc=$request->query->get('pc');
       if($pn!=$pc ){
        echo json_encode(array(
                'Mess' => 'Mật khẩu không trùng khớp !',
                'Style' => 'border:red solid 1px'
            ),JSON_UNESCAPED_UNICODE);
            exit();
         
        }
        else
            echo json_encode(array(
                'Mess' => '',
                'Style' => 'border:green solid 1px'
            ),JSON_UNESCAPED_UNICODE);
            exit();
    }  


   /**
     * @Route("/forgetpass", name="forgetpass")
     */
    public function forgetpassAction(Request $request)
     {  
      $session = new Session();
       if($request->isMethod('POST')){   
//dump($request);
            $errors=array();
            $acc=trim($request->get('accountt'));
            $user= UserQuery::create()->filterByAccount($acc)->findOne();
          
            if($user!=null){
            if($request->get('name')=='')
                 $errors['name']='Không được để trống chủ tài khoản ! ';
            if(trim($request->get('name'))!=$user->getName())
               $errors['name']='Tên tài khoản không đúng ! ';
            
            if(trim($request->get('email'))!=$user->getEmail())
               $errors['email']='Email không đúng ! ';

            if(strlen($request->get('passnew'))<5 ){
                $errors['password']='Mật khẩu ít nhất phải 5 kí tự';        
            }
            if(trim($request->get('email'))=='')
                 $errors['email']='Không được để trống email ! ';
            if($request->get('passnew')=='')
                 $errors['password']='Không được để trống mật khẩu ! ';
            if($request->get('confirmpassnew')=='')
                 $errors['confirmpassword']='Phải nhập lại mật khẩu ! ';
            if($request->get('confirmpassnew')!=$request->get('passnew'))
                 $errors['confirmpassword']='Mật khẩu nhập lại không khớp ! ';
            if($request->get('captcha')=='' || $request->get('captcha')!=$session->get('code'))
                 $errors['captcha']='Mã bảo vệ không đúng ! ';
           
             if(!$errors){
                // dump($session->getFlashBag()->get('advertise'));die;
                $user->setPassword($request->get('passnew'));
                $user->save();
                $session->set('account', $acc);
                $session->set('makh',$user->getMakh());
                $session->set('name',$user->getName());
                $session->set('date',$user->getDateregister());
                $session->set('state',$user->getState());
                $session->getFlashBag()->add('advertise','Mật khẩu đã được thay đổi ! ');
               return $this->redirectToRoute('homepage');
           
            }


              $this->view_data['name']=  $request->get('name');
              $this->view_data['email']=$request->get('email');
            
              $this->view_data['errors']=$errors;
                return $this->render('@App/forgetpass.html.twig',$this->view_data);
                
            }

            else{

              $this->view_data['name'] =  $request->get('name');
              $this->view_data['email']=$request->get('email');
              $this->view_data['accountt']=$request->get('account');
              $this->view_data['erracc']='Tên đăng nhập không đúng !';
                return $this->render('@App/forgetpass.html.twig',$this->view_data);

            }
            
           
       }
       if( $session->get('account')!=null)
       {
        return $this->redirectToRoute('homepage');
       }
        return $this->render('@App/forgetpass.html.twig');
       
    }
     /**
     * @Route("/info_account", name="info_account")
     */
    public function infoAccountAction(Request $request)
    {
        $session=new Session();
         if( $session->get('account')==null)
       {
        return $this->redirectToRoute('homepage');
       }
       
      
        $p=40;
    
        $this->view_data['acc']=$session->get('account');
        $this->view_data['makh']=$makh=$session->get('makh');
        $this->view_data['date']=$date=$session->get('date');
        $this->view_data['state']=$state=$session->get('state');
        $user=UserQuery::create()->filterByMakh($makh)->findOne();
        $this->view_data['user']=$user;
        $this->view_data['name']=$name=$user->getName();
        if($user->getName()!='')
            $p=$p+7.5;
        if($user->getGender()!='')
            $p=$p+7.5;
        if($user->getBirthday()!='')
            $p=$p+7.5;
        if($user->getAvatar()!='')
            $p=$p+7.5;
        if($user->getDescribe()!='')
            $p=$p+7.5;
        if($user->getAddress()!='')
            $p=$p+7.5;
        if($user->getPhonenumber()!='')
            $p=$p+7.5;
        if($user->getState()!='')
            $p=$p+7.5;
       
         $this->view_data['percent']=$p;
        
        $advertise=$session->getFlashBag()->get('advertise');
        if($advertise)
          $this->view_data['advertise']= $advertise[0];
         //dump($user);die;
        return $this->render('@App/info_acc.html.twig',$this->view_data);
       
      
        
    }
     /**
     * @Route("/update_info", name="update_info")
     */
    public function update_info(Request $request)
     { 
        $session=new Session();
        if( $session->get('account')==null)
       {
        return $this->redirectToRoute('homepage');
       }
         $files = $request->files->get('anh'); 
        $this->view_data['acc']=$session->get('account');
        $this->view_data['makh']=$makh=$session->get('makh');
        $this->view_data['date']=$session->get('date');
        $this->view_data['name']=$name=$session->get('name');
         $this->view_data['state']=$state=$session->get('state');
        $user=UserQuery::create()->filterByMakh($makh)->findOne();
        $this->view_data['user']=$user;
       
        if($request->isMethod('POST')){
            $user->setGender(($request->get('gioitinh')!=null)?$request->get('gioitinh'):'' );
            $user->setName(($request->get('hoten')!=null)?$request->get('hoten'):'' );
            $user->setBirthday(($request->get('ngaysinh')!=null)?$request->get('ngaysinh'):'' );
            $user->setAddress(($request->get('diachi')!=null)?$request->get('diachi'):'' );
            $user->setPhonenumber(($request->get('sdt')!=null)?$request->get('sdt'):'' );
           
            $user->setAvatar($files!=null?$files->getClientOriginalName():$user->getAvatar());
           // $user->setDescribe(($request->get('gioithieu')!=null)?$request->get('gioithieu'):'' );
        
            if(strlen($request->get('gioithieu'))>230){
               $session->getFlashBag()->add('advertise','Cập nhật thông tin thất bại ! ');
              return $this->redirectToRoute('info_account');
          }
          if($files!=null)
          move_uploaded_file($files->getPathName(), 'userfiles/'.$files->getClientOriginalName());
          $user->setDescribe(($request->get('gioithieu')!=null)?$request->get('gioithieu'):'' );
          $user->save();
          $session->getFlashBag()->add('advertise','Cập nhật thông tin thành công ! ');
          return $this->redirectToRoute('info_account');
          
        }
        
        return $this->render('@App/info_acc.html.twig',$this->view_data);
     }

      /**
     * @Route("/gender_error", name="gender_error")
     */
    public function gender_error(Request $request)
     {   
       $gt=$request->query->get('gt');
       if(strlen($gt)>230 ){
           echo 'Nội dung nhập vào không được quá 230 ký tự !';
          exit();
        }
        else
            exit();
    }



}

