<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use AppBundle\Model\UserQuery;
use AppBundle\Model\User;
class RegisterController extends Controller
{
	 /**
     * @Route("/register", name="user_register")
     */
    public function registerAction(Request $request,\Swift_Mailer $mailer)
     { 
       $session = new Session();
       if($request->isMethod('POST')){       		
            $errors=array();
            $user = new User();
            
        	$user->setName($request->get('name')!=null ? trim($request->get('name')):'');
    		$user->setAccount($request->get('account')!=null ? trim($request->get('account')):'');
            $user->setEmail($request->get('email') !=null ? trim($request->get('email')) :'');
            $user->setPassword($request->get('passnew')!=null?$request->get('passnew'):'');
            $user->setDateregister(date('Y-m-d'));
            $user->setState(false);
            $user->setDelete(true);
            
            if($request->get('account')=='')
                 $errors['account']='Không được để trống tên đăng nhập ! ';
            $check_account= UserQuery::create()->filterByAccount($request->get('account'))->findOne();

            if ($check_account != null) {
                $errors['account'] = "Tên tài khoản đã được sử dụng !";
            }

            if (!preg_match("/^[a-zA-Z0-9_ ]*$/",$request->get('account'))) {
                $errors['account']= 'Tên đăng nhập chỉ bao gồm chữ số từ 0-9,_ và chữ cái a-z,A-Z';
            
            }
            if(strlen($request->get('account'))<6 || strlen($request->get('account'))>20){
                $errors['account']= 'Tên đăng nhập phải từ 6-20 kí tự';
                
            }
            if(strlen($request->get('passnew'))<5 ||strlen($request->get('passnew'))>30 ){
                $errors['password']='Mật khẩu phải từ 5 đến 30 kí tự !';        
            }
            $email=trim($request->get('email'));
            $check_email= UserQuery::create()->filterByEmail($email)->findOne();
            if ($check_email != null) {
                $errors['email'] = "Email đã được sử dụng !";
            }
            if (!filter_var($request->get('email'), FILTER_VALIDATE_EMAIL)) {
                $errors['email']='Định dạng email không hợp lệ ! ';
            }
            if($request->get('email')=='')
                 $errors['email']='Không được để trống email ! ';
            if($request->get('passnew')=='')
                 $errors['password']='Không được để trống password ! ';
            if($request->get('confirmpassnew')=='')
                 $errors['confirmpassword']='Phải nhập lại mật khẩu ! ';
            if($request->get('confirmpassnew')!=$request->get('passnew'))
                 $errors['confirmpassword']='Mật khẩu nhập lại không khớp ! ';
           
      
            if(!$errors){
                $session->set('account', $request->get('account'));
                $makh= $session->set('makh',$user->getMakh());
                $date= $session->set('date', $user->getDateregister());
                 $session->set('state', $user->getState());
                  $session->set('name', $user->getName());
                      //gui mail

                $activation=md5($email.time()); 
                $user->setCode($activation);
                 $user->save();
                 $message = (new \Swift_Message('Hello Email'))
                ->setSubject('Verify email')
                ->setFrom('den.a1.hn@gmail.com')
                ->setTo('hanhnguyen060497@gmail.com')
                ->setBody(
                     "Hi " . $email . "!" .PHP_EOL. "Please click the link below to verify your subscription 
                    http://127.0.0.1:8000/activate/".$activation
                )
                
               ;
               $mailer->send($message);

               $session->getFlashBag()->add('advertise','Bạn đã đăng ký thành công, Hãy kiểm tra email để kích hoạt tài khoản');
                



                return $this->redirectToRoute('homepage');    
            }
            else{
                return $this->render('@App/register.html.twig',['user'=>$user,'errors'=>$errors]);

            }
           
       }
       if( $session->get('account')!=null)
       {
        return $this->redirectToRoute('homepage');
       }
        return $this->render('@App/register.html.twig');
    }
}