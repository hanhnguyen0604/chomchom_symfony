$(document).ready(function() {
	  var timeout = null;
	$('#signout-btn').click(function() {
		$('.logout').removeClass('in');
		$('.login').addClass('in');
	});
	$('.dropdowns').click(function() {
		$('.menu').toggleClass('out');
	});

  $('body').on('keyup','.cpassold',function(){ 
  	clearTimeout(timeout);
    timeout = setTimeout(function ()
    {  
  	var po = $('.cpassold').val();

   $.get('/passwordold?po='+po,function(data){ 
   	var getData = $.parseJSON(data);
        $('#notipassold').text(getData.Mess);
        $('.cpassold').attr('style',getData.Style);
       
    });
     }, 500);

 });
    $('body').on('keyup','.cpassnew',function(){ 
  	clearTimeout(timeout);
    timeout = setTimeout(function ()
    {  
  	var pn = $('.cpassnew').val();
    var po = $('.cpassold').val();
   $.get('/passwordnew?pn='+pn+'&po='+po,function(data){ 
   	 var getData = $.parseJSON(data);
        $('#notipassnew').text(getData.Mess);
        $('#notipassnew').attr('style',getData.Color);
        $('.cpassnew').attr('style',getData.Style);
       
    });
     }, 500);

 });
    $('body').on('keyup','.cconfirmpass',function(){ 
  	clearTimeout(timeout);
    timeout = setTimeout(function()
    {  
  	var pn = $('.cpassnew').val();
  	var pc = $('.cconfirmpass').val();

   $.get('/confirmpass?pn='+pn+'&pc='+pc,function(data){
   	var getData = $.parseJSON(data);
        $('#noticonfirmpass').text(getData.Mess);

        $('.cconfirmpass').attr('style',getData.Style);
    });
     }, 500);

 });

    $('body').on('keyup','#gioithieu',function(){ 
    clearTimeout(timeout);
    timeout = setTimeout(function ()
    {  
    var gt = $('#gioithieu').val();
   $.get('/gender_error?gt='+gt,function(data){ 
    
        $('#gt-error').text(data);
    });
     }, 500);

 });

});